#include "samplewidget.h"

#define SAMPLEWIDGET_UPDATE_INTERVAL    20  //[ms] Refresh interval for the widget

Samplewidget::Samplewidget(Controller* controller, QWidget* parent) :
    QWidget(parent),
    ui(new Ui::Samplewidget)
{
    ui->setupUi(this);
    
    // initialize widgets
    update_timer = new QTimer(this);
    p_controller = controller;

    // connect signals and slots
    connect(update_timer,   SIGNAL(timeout()), this, SLOT(refresh()));
    connect(ui->pb_connect, SIGNAL(clicked()), this, SLOT(on_button_clicked()));

    // start update timer
    update_timer->start(SAMPLEWIDGET_UPDATE_INTERVAL);
}

Samplewidget::~Samplewidget()
{
    delete ui;
}

//
// SLOT: connect button clicked
//
void Samplewidget::on_button_clicked()
{
    int state = p_controller->sensor()->state();

    if(state == SENSORIF_DISCONNECTED)
        p_controller->connect("127.0.0.1","8599");

    if(state == SENSORIF_CONNECTED)
        p_controller->disconnect();
}


void Samplewidget::refresh()
{
    // update counter
    static int count = 0;
    ui->lb_counter->setText(QString::number(count++));

    // update state label (update label only if different from the previous one
    static int prev_state = -1;
    int state = p_controller->sensor()->state();

    if (state != prev_state)
    {
        switch (state)
        {
        case SENSORIF_DISCONNECTED:
            ui->lb_state->setText("DISCONNECTED");
            ui->pb_connect->setEnabled(true);
            ui->pb_connect->setText("CONNECT");
            break;
        case SENSORIF_CONNECTING:
            ui->lb_state->setText("CONNECTING");
            ui->pb_connect->setEnabled(false);
            ui->pb_connect->setText("CONNECTING");
            break;
        case SENSORIF_CONNECTED:
            ui->lb_state->setText("CONNECTED");
            ui->pb_connect->setEnabled(true);
            ui->pb_connect->setText("DISCONNECT");
            break;
        }

        prev_state = state;
    }
}

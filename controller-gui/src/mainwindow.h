#pragma once

#include <QMainWindow>

#include "ui_mainwindow.h"
#include "samplewidget.h"
#include "controller.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(Controller* controller, QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow* ui;
    Samplewidget*   sample_widget;
    Controller*     p_controller;
};


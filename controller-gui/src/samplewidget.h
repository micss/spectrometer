#pragma once

#include <QWidget>
#include <QTimer>

#include "ui_samplewidget.h"
#include "controller.h"

namespace Ui {
class Samplewidget;
}

class Samplewidget : public QWidget
{
    Q_OBJECT

public:
    explicit Samplewidget(Controller* controller, QWidget* parent = 0);
    ~Samplewidget();

private slots:
    void refresh();
    void on_button_clicked();

private:
    Ui::Samplewidget*   ui;
    QTimer*             update_timer;
    Controller*         p_controller;
};

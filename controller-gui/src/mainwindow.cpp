#include "mainwindow.h"

MainWindow::MainWindow(Controller* controller, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // initialize mainwin
    p_controller = controller;

    // create and add all the widgets programmatically
    sample_widget = new Samplewidget(p_controller);
    ui->layout_top->addWidget(sample_widget);
}

MainWindow::~MainWindow()
{
    delete ui;
}

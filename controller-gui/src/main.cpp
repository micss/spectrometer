#include <QApplication>
#include "controller.h"
#include "mainwindow.h"


int main(int argc, char *argv[])
{
    QApplication app (argc, argv);

    // create controller
    Controller* ctrl = new Controller();

    // create main window passing the pointer to the controller
    MainWindow mainwin(ctrl);
    mainwin.show();

    return app.exec();
}

#include "hashing/crc.h"

//TODO include "utils\bit_operations.h"   reverse

namespace mlib {

    // TODO: define CRC tables for faster calculation

    // initialize global variables with defaults
    // ..

    // private sructures
    // ..

    inline void reflect(uint8_t &data)              // TODO: move this to bitwise_operations.h
    {
        uint8_t temp = 0x00;
        for (int bit = 0; bit < 8; ++bit)
        {
            if (data & 0x01) {
                temp |= (1 << (7 - bit));
            }
            data = (data >> 1);
        }
        data = temp;
    }

    inline void reflect(uint16_t &data)
    {
        uint16_t temp = 0x0000;
        for (int bit = 0; bit < 16; ++bit)
        {
            if (data & 0x01) {
                temp |= (1 << (15 - bit));
            }
            data = (data >> 1);
        }
        data = temp;
    }

    //
    //  CRC 8 bits - Calculate using default CRC algorithm
    //
    uint8_t CRC8::calculate(const char* buffer, size_t size)
    {
        return CRC8::calculate(buffer, size, CRC_8);                // TODO: use define for default CRC8 type
    }

    //
    //  CRC 8 bits - Calculate using a specific CRC algorithm, with default parameters
    //
    uint8_t CRC8::calculate(const char* buffer, size_t size, CRC8Type crctype)
    {
        switch (crctype)
        {
        case CRC_8:
            return CRC8::calculate(buffer, size, 0x07, 0x00, 0x00, false, false);
        case CRC_8_CDMA2000:
            return CRC8::calculate(buffer, size, 0x9B, 0xFF, 0x00, false, false);
        case CRC_8_DARC:
            return CRC8::calculate(buffer, size, 0x39, 0x00, 0x00, true, true);
        case CRC_8_DVBS2:
            return CRC8::calculate(buffer, size, 0xD5, 0x00, 0x00, false, false);
        case CRC_8_EBU:
            return CRC8::calculate(buffer, size, 0x1D, 0xFF, 0x00, true, true);
        case CRC_8_ICODE:
            return CRC8::calculate(buffer, size, 0x1D, 0xFD, 0x00, false, false);
        case CRC_8_MAXIM:
            return CRC8::calculate(buffer, size, 0x31, 0x00, 0x00, true, true);
        default:
            break;
        }

        return 0;
    }

    //
    //  CRC 8 bits - Calculate using a specific CRC algorithm, with default parameters
    //
    uint8_t CRC8::calculate(const char* buffer, size_t size, uint8_t poly, uint8_t init, uint8_t xorout, bool refin, bool refout)
    {
        uint8_t crc = init;
        for (int p = 0; p < size; p++)
        {
            uint8_t byte = buffer[p];           
            if (refin) reflect(byte);
            crc ^= (byte);                 

            for (int i = 0; i < 8; i++)
            {
                if (crc & 0x80) {               // crc & TOPBIT --> TOPBIT : (1 << (WIDTH - 1)) : (1 << 7) : 0x4000
                    crc = (crc << 1) ^ poly;    // TODO, mask using 0xFF
                }
                else {
                    crc = (crc << 1);           // TODO, mask using 0xFF
                }
            }
        }

        if (refout) reflect(crc);
        return crc ^ xorout;
    }

    //
    //  CRC 16 bits - Calculate using default CRC algorithm
    //
    uint16_t CRC16::calculate(const char* buffer, size_t size)
    {
        return CRC16::calculate(buffer, size, CRC_16_CCITT);            // TODO: use define for default CRC16 type
    }

    //
    //  CRC 16 bits - Calculate using a specific CRC algorithm, with default parameters
    //
    uint16_t CRC16::calculate(const char* buffer, size_t size, CRC16Type crctype)
    {
        switch (crctype)
        {
        case CRC_16_CCITT:
            return CRC16::calculate(buffer, size, 0x1021, 0x1D0F, 0x0000, false, false);
        case CRC_16_X25:
            return CRC16::calculate(buffer, size, 0x1021, 0xffff, 0xffff, true, true);
        case CRC_16_XMODEM:
            return CRC16::calculate(buffer, size, 0x1021, 0x0000, 0x0000, false, false);
        case CRC_16_KERMIT:
            return CRC16::calculate(buffer, size, 0x1021, 0x0000, 0x0000, true, true);
        case CRC_16_CCITTFALSE:
            return CRC16::calculate(buffer, size, 0x1021, 0xffff, 0x0000, false, false);
        case CRC_16_CDMA2000:
            return CRC16::calculate(buffer, size, 0xc867, 0xffff, 0x0000, false, false);
        default:
            break;
        }

        return 0;
    }

    //
    //  CRC 16 bits - Calculate using a specific CRC algorithm, with custom parameters
    //
    uint16_t CRC16::calculate(const char* buffer, size_t size, uint16_t poly, uint16_t init, uint16_t xorout, bool refin, bool refout)
    {
        // TODO: LAZY INIT of CRC Table (vector??)
        // ...

        uint16_t crc = init;
        for (int p = 0; p < size; p++)
        {
            uint8_t byte = buffer[p];           // TODO, mask using 0xFFFF      // use while(size--) *buffer++
            if (refin) reflect(byte);           // TODO: use inline, MACRO
            crc ^= (byte) << 8;                 // crc ^= (REFLECT_DATA(buffer[p]) << (WIDTH - 8));

            for (int i = 0; i < 8; i++)
            {
                if ( crc & 0x8000) {            // crc & TOPBIT --> TOPBIT : (1 << (WIDTH - 1)) : (1 << 15) : 0x8000
                    crc = (crc << 1) ^ poly;    // TODO, mask using 0xFFFF
                }
                else {
                    crc = (crc << 1);           // TODO, mask using 0xFFFF
                }
            }
        }

        if (refout) reflect(crc);
        return crc ^ xorout;
    }

}

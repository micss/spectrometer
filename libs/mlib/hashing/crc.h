#pragma once

//  references: https://en.wikipedia.org/wiki/Cyclic_redundancy_check
//              http://reveng.sourceforge.net/crc-catalogue/16.htm

//  Implemented CRC Algorithms:
//
//  -----------------------------------------------------------------------------------------------------------
//  CRC8                    CRC-8       CDMA2000    DARC        DVB-S2      EBU         I-CODE      MAXIM
//  -----------------------------------------------------------------------------------------------------------
//  Width                   8 bits      8 bits      8 bits      8 bits      8 bits      8 bits      8 bits
//  Polynomial              0x07        0x9B        0x39        0xD5        0x1D        0x1D        0x31
//  Initial Remainder       0x00        0xFF        0x00        0x00        0xFF        0xFD        0x00
//  Final XOR value         0x00        0x00        0x00        0x00        0x00        0x00        0x00
//  Reflected data          No          No          Yes         No          Yes         No          Yes
//  Reflected reminder      No          No          Yes         No          Yes         No          Yes
//
//  -----------------------------------------------------------------------------------------------------------
//  CRC16                   AUG/CCITT   X25         XMODEM      KERMIT      CCITTFALSE  CDMA2000 
//  -----------------------------------------------------------------------------------------------------------
//  Width                   16 bits     16 bits     16 bits     16 bits     16 bits     16 bits
//  Polynomial              0x1021      0x1021      0x1021      0x1021      0x1021      0xC867
//  Initial Remainder       0x1D0F      0xFFFF      0x0000      0x0000      0xFFFF      0xFFFF
//  Final XOR value         0x0000      0xFFFF      0x0000      0x0000      0x0000      0x0000
//  Reflected data          No          Yes         No          Yes         No          No
//  Reflected reminder      No          Yes         No          Yes         No          No          
//
//
//  Error detection rates:
//  ----------------------
//   8-bit: 99.6094%
//  16-bit: 99.9985%
//  32-bit: 99.9999%


#include <stdint.h>
#include <stddef.h> // size_t
#include <vector>

namespace mlib {

    enum CRC8Type  { CRC_8, CRC_8_CDMA2000, CRC_8_DARC, CRC_8_DVBS2, CRC_8_EBU, CRC_8_ICODE, CRC_8_MAXIM };
    enum CRC16Type { CRC_16_CCITT, CRC_16_X25, CRC_16_XMODEM, CRC_16_KERMIT, CRC_16_CCITTFALSE, CRC_16_CDMA2000 };
    enum CRC32Type { CRC_32, CRC_32C, CRC_32K, CRC_32Q };

    class CRC8
    {
    public:
        // static uint8_t fast(const char* buffer, int size);           //TODO implement fast crc computing with static tables (generated using the default algorithm at init)
        static uint8_t calculate(const char* buffer, size_t size);
        static uint8_t calculate(const char* buffer, size_t size, CRC8Type crctype);
        static uint8_t calculate(const char* buffer, size_t size, uint8_t polynomial, uint8_t init, uint8_t xorout, bool refin, bool refout);
    };

    class CRC16
    {
    public:
        //static uint16_t fast(const char* buffer, int size);           //TODO implement fast crc computing with static tables (generated using the default algorithm at init)
        static uint16_t calculate(const char* buffer, size_t size);
        static uint16_t calculate(const char* buffer, size_t size, CRC16Type crctype);
        static uint16_t calculate(const char* buffer, size_t size, uint16_t polynomial, uint16_t init, uint16_t xorout, bool refin, bool refout);
    };
}

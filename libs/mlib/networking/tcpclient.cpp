#include "networking/tcpclient.h"

#define CONNECTION_TIMEOUT  3       // [s] Timeout after which to kill a connection attempt
#define RECEIVE_TIMEOUT     1000    // [ms] recv() timeout
#define RECEIVE_BUFFER_SIZE 2048    // [bytes] size of the recv() temp buffer

#ifdef MLIBWIN
#define CLOSE_SOCKET(S) closesocket((S));
#else
#define CLOSE_SOCKET(S) close((S));
#define SOCKET_ERROR    -1
#define INVALID_SOCKET  -1
#endif

namespace mlib {

    enum TCPClientState { TCPCLIENT_INITIALIZING, TCPCLIENT_DISCONNECTED, TCPCLIENT_CONNECTING, TCPCLIENT_CONNECTED, TCPCLIENT_ERROR, TCPCLIENT_INITFAILED, TCPCLIENT_NUM_STATES };
    const char* tcpclient_state_name[] = { "TCPCLIENT_INITIALIZING", "TCPCLIENT_DISCONNECTED", "TCPCLIENT_CONNECTING", "TCPCLIENT_CONNECTED", "TCPCLIENT_ERROR", "TCPCLIENT_INITFAILED" };

    //
    //  CONSTRUCTOR
    //
    TCPClient::TCPClient()
    {
        m_state = TCPCLIENT_INITIALIZING;
        initialize_socket();
    }

    //
    //  DESTRUCTOR
    //
    TCPClient::~TCPClient()
    {
        // if m_socket != INVALID_SOCKET
        CLOSE_SOCKET(m_socket);
#ifdef MLIBWIN
        WSACleanup();
#endif
    }


    //
    //  INITIALIZE SOCKET
    //  initialize memory structures for a TCP stream socket
    //
    void TCPClient::initialize_socket()
    {
        m_socket = INVALID_SOCKET;
        p_server_addr = NULL;

#ifdef MLIBWIN
        // WINDOWS PLATFORM
        int rc = WSAStartup(MAKEWORD(2, 0), &wsaData);
        if (rc != 0) {
            printf("TCPClient> WSAStartup failed \n");
            m_state = TCPCLIENT_INITFAILED;
            return;
        }

        ZeroMemory(&hints, sizeof(hints));
#else
        // UNIX PLATFORM
        memset(&hints, 0, sizeof hints);
#endif
        hints.ai_family   = AF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_protocol = IPPROTO_TCP;

        m_state = TCPCLIENT_DISCONNECTED;
    }


    //
    //  OPEN TCP CONNECTION
    //  Spawn a thread and try to enstablish connection with the server.
    //  This call is non-blocking, at the end of this operation the TCP Client state
    //  will change (either in CONNECTED or CONNECTION_ERROR).
    //
    void TCPClient::open_connection(std::string ip_addr, std::string port)
    {
        m_ip_addr = ip_addr;
        m_port = port;

        if (m_state == TCPCLIENT_DISCONNECTED)         // TODO: verify socket is always reinitialize when it goeas into ERROR state or INITERROR
        {
            m_state = TCPCLIENT_CONNECTING;
            p_connect_thread = new std::thread(&TCPClient::connection_thread, this);
        }
    }

    //
    //  CLOSE TCP CONNECTION
    //  If the socket is connected, the connection is closed. If the socket is busy enstablishing
    //  a connection, the process is terminated and the socket left in a READY state.
    //
    void TCPClient::close_connection()
    {
        // TODO: check the current state, if connecting: wait or stop the thread
        // TODO: Handle case of CONNECTING state in which there is a running thread trying to enstablish connection

        if (m_state == TCPCLIENT_CONNECTED)
        {
            CLOSE_SOCKET(m_socket);     //TODO: check if this also set m_socket to -1
            m_state = TCPCLIENT_DISCONNECTED;
        }
    }

    //
    //  CONNECTION THREAD
    //  Try to enstablish connection with the server in background using a thread.
    //  At the end of the operation. it changes the TCP client state to either CONNECTED or ERROR.
    //
    void TCPClient::connection_thread()
    {
        printf("TCPClient> trying to connect to [%s] on port [%s] \n", m_ip_addr.data(), m_port.data());

#ifdef MLIBWIN

		// Resolve the server address and port
		int rc = getaddrinfo(m_ip_addr.data(), m_port.data(), &hints, &p_server_addr);
		if (rc != 0) {
			// TODO: log last socket error
			fprintf(stderr, "TCPClient> getaddrinfo() failed: %s\n", gai_strerror(rc));
			return;
		}

		// loop through the linked list of results and connect to the first we can
		for (p = p_server_addr; p != NULL; p = p->ai_next)
		{
			m_socket = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
			if (m_socket == INVALID_SOCKET) {
				perror("TCPClient> socket() failed");
				continue;
			}

			// make socket reusable
			BOOL yes = TRUE;
			if (setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR, (char*)&yes, sizeof(yes)) == SOCKET_ERROR) {
				printf("TCPClient> setsockopt() failed setting ReuseAddr\n");
				continue;
			}

			// Set non-blocking
			unsigned long on = 1;
			if (0 != ioctlsocket(m_socket, FIONBIO, &on)) {
				printf("TCPClient> Failed to set socket to NON blocking \n");
				continue;
			}

			// connect to the server
			rc = connect(m_socket, p->ai_addr, (int)p->ai_addrlen);

			if (rc == SOCKET_ERROR)
			{
                // if the connection attempt is started
                if (WSAGetLastError() == WSAEWOULDBLOCK)
                {
                    int err = 0;
                    int len = sizeof(int);

                    fd_set  wset, eset;
                    TIMEVAL timeout;

                    FD_ZERO(&wset);
                    FD_ZERO(&eset);
                    FD_SET(m_socket, &wset);
                    FD_SET(m_socket, &eset);
                    timeout.tv_sec = CONNECTION_TIMEOUT;
                    timeout.tv_usec = 0;

                    rc = select(m_socket+1, NULL, &wset, &eset, &timeout);

                    if (rc == 0) {
                        printf("TCPClient> Connect timeout \n");
                        CLOSE_SOCKET(m_socket);
                        continue;
                    }
                    if (rc < 0) {
                        getsockopt(m_socket, SOL_SOCKET, SO_ERROR, (char*)(&err), &len);
                        printf("TCPClient> Select error [code: %d]: %s\n", err, strerror(err));
                        CLOSE_SOCKET(m_socket);
                        continue;
                    }
                    if (rc > 0)	{
                        // TODO: check FD_ISSET(m_socket, &wset) to see is socket is writable
                        // TODO: check FD_ISSET(m_socket, &eset) for errors

                        getsockopt(m_socket, SOL_SOCKET, SO_ERROR, (char*)(&err), &len);
                        if (err)
                        {
                            printf("TCPClient> Connection error [code: %d]: %s\n", err, strerror(err));
                            CLOSE_SOCKET(m_socket);
                            continue;
                        }

                        printf("TCPClient> connection enstablished\n");
                        m_state = TCPCLIENT_CONNECTED;
                    }
                }
                else
                {
                    printf("TCPClient> connect() error \n");
                    CLOSE_SOCKET(m_socket);
                    continue;
                }
            }

            // make socket blocking again
            unsigned long off = 0;
            if (0 != ioctlsocket(m_socket, FIONBIO, &off)) {
	            printf("Failed to set socket to blocking");
			}

			break;
		}

		if (p == NULL) {
			fprintf(stderr, "TCPClient> failed to connect\n");
			m_state = TCPCLIENT_DISCONNECTED;	// TODO: error state?
		}

        // free memory
        freeaddrinfo(p_server_addr);

#else

        // Resolve the server address and port
        int rc = getaddrinfo(m_ip_addr.data(), m_port.data(), &hints, &p_server_addr);
        if (rc != 0) {
            fprintf(stderr, "TCPClient> getaddrinfo() failed: %s\n", gai_strerror(rc));
            return;
        }

        // loop through the linked list of results and connect to the first we can
        for(p = p_server_addr; p != NULL; p = p->ai_next)
        {
            m_socket = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
            if (m_socket == INVALID_SOCKET) {
                perror("TCPClient> socket() failed");
                continue;
            }

            // make socket reusable
            // TODO ..

            // make socket non blocking
            int flags = fcntl(m_socket, F_GETFL, 0);
            fcntl(m_socket, F_SETFL, flags | O_NONBLOCK);

            // perfom a connect on a non-blocking socket using select()
            // when a user defined timeout is reached, interrupt the connection attempt
            rc = connect(m_socket, p->ai_addr, p->ai_addrlen);

            if (rc < 0)
            {
                if (errno == EINPROGRESS)
                {
                    struct timeval timeout;
                    fd_set wset;
                    int valopt;

                    timeout.tv_sec  = CONNECTION_TIMEOUT;
                    timeout.tv_usec = 0;
                    FD_ZERO(&wset);
                    FD_SET(m_socket, &wset);

                    if (select(m_socket+1, NULL, &wset, NULL, &timeout) > 0)
                    {
                        socklen_t lon = sizeof(int);
                        getsockopt(m_socket, SOL_SOCKET, SO_ERROR, (void*)(&valopt), &lon);

                        if (valopt)
                        {
                            fprintf(stderr, "TCPClient> Error in connection() %d - %s\n", valopt, strerror(valopt));
                            CLOSE_SOCKET(m_socket);
                            continue;
                        }
                        else
                        {
                            m_state = TCPCLIENT_CONNECTED;
                        }
                    }
                    else
                    {
                        fprintf(stderr, "TCPClient> Timeout or error() %d - %s\n", valopt, strerror(valopt));
                        CLOSE_SOCKET(m_socket);
                        continue;
                    }
                }
                else
                {
                    fprintf(stderr, "TCPClient> Error connecting %d - %s\n", errno, strerror(errno));
                    CLOSE_SOCKET(m_socket);
                    continue;
                }
            }

            // make socket blocking again if connection was successful
            fcntl(m_socket, F_SETFL, flags & (~O_NONBLOCK));

            break;
        }

        if (p == NULL) {
            fprintf(stderr, "TCPClient> failed to connect\n");
            m_state = TCPCLIENT_DISCONNECTED;       // TODO: use the TCPCLIENT_ERROR state?
        }

        // free memory
        freeaddrinfo(p_server_addr);
#endif
    }


    //
    //  WRITE DATA
    //
    int TCPClient::read(char* buffer, size_t num_bytes)
    {
        int bytes_received = recv(m_socket, buffer, num_bytes, 0);

        if (bytes_received == 0) {
            printf("TCPServer> Connection close by peer\n");
        }

        if (bytes_received < 0) {
            printf("TCPServer> Recv() failed\n");
        }

        return bytes_received;
    }

    //
    //  READ DATA
    //
    int TCPClient::write(char* buffer, size_t num_bytes)
    {
        int rc, bytes_sent = 0;

        while (bytes_sent < num_bytes)
        {
            rc = send(m_socket, buffer+bytes_sent, num_bytes-bytes_sent, 0);

            if (rc == SOCKET_ERROR) {
                printf("TCPClient> send failed\n");
                // check socket error TODO: handle partial sends
                // ..
                return RC_ERROR;
            }

            bytes_sent += rc;
        }

        return RC_OK;
    }


    //
    //  GETTERS
    //
    bool TCPClient::connected()
    {
        return (m_state == TCPCLIENT_CONNECTED) ? true : false;
    }
    bool TCPClient::disconnected()
    {
        return (m_state == TCPCLIENT_DISCONNECTED) ? true : false;
    }
}

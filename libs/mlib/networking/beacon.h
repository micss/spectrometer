#pragma once

#include <string>
#include <vector>
#include <thread>

#include "foundation/defines.h"
#include "networking/udpsocket.h"

#define BEACON_DEFAULT_PORT         10549
#define BEACON_TIMEOUT_HEARTBEAT    3000    // [ms] timeout for heartbeat messages
#define BEACON_ID_LENGTH            6       //      number of hex digits used in beacon_id

namespace mlib {

    struct t_node
    {
        std::string beacon_id;
        std::string hostname;
        std::string ip_addr;
        // local node date, time, timezone, locale
        int         time_offset;    // [s] difference in time between the local and remote wall clock
        int         last_seen;      // [s] last time a message was received from remote node (local wall clock)
        // list/vector of node properties
    };

    class Beacon
    {
    public:
        Beacon();
        ~Beacon();
        // methods
        void make_discoverable(bool discoverable);
        void set_node_property(std::string key, std::string value);
        void set_node_property(std::string key, int value);
        void trigger();
        // getters
        std::vector<t_node>* node_list() { return &m_node_list; }

    private:
        bool                m_discoverable;
        bool                m_listening;
        t_node              m_local_node;
        std::vector<t_node> m_node_list;
        UDPSocket*          p_udp_socket;
        std::thread*        p_listener_thread;
        std::thread*        p_heartbeat_thread;
        // threads
        void listener();
        void heartbeat();
        // methods
        std::string generate_beacon_id();
        void update_last_seen(std::string beacon_id);
    };

}

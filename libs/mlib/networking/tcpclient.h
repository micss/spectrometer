#pragma once

#ifdef MLIBWIN
#include <winsock2.h>               // Winsock Library v2.0
#include <ws2tcpip.h>               // getaddrinfo
#pragma comment(lib,"ws2_32.lib")
#else
#include <string.h>                 // memset() strcpy()
#include <unistd.h>                 // close()
#include <arpa/inet.h>              // struct sockaddr_in
#include <sys/types.h>              // struct addrinfo
#include <sys/socket.h>             // socket()
#include <sys/time.h>               // struct timeval
#include <netdb.h>
#include <fcntl.h>                  // fcntl() F_SETFL
//#include <stdio.h>
//#include <stdlib.h>
//#include <errno.h>
//#include <netinet/in.h>
#endif

#include <thread>
#include <mutex>

#include "foundation/defines.h"

namespace mlib {

    class TCPClient
    {
    public:
        TCPClient();
        ~TCPClient();
        // methods
        void open_connection(std::string ip_addr, std::string port);    // open a TCP connection, not blocking
        void close_connection();                                        // terminate and close connection
        int  write(char* buffer, size_t num_bytes);                     // send data and returns, blocking call
        int  read(char* buffer, size_t num_bytes);                      // receive n btyes, blocking call
        // getters
        bool connected();
        bool disconnected();
        // setters
        // void set_autoreconnect(bool reconect);
        // void set_connection_timeout(unsigned int timeout_sec);
        // void set_buffer_size(size_t buffer_size);

    private:
        int             m_state;
        std::thread*    p_connect_thread;
        std::mutex      m_mutex;
        std::string     m_ip_addr;
        std::string     m_port;

        // auto reconnect
        // connection timeout
        // receive timeout
        // buffer size

#ifdef MLIBWIN
        WSADATA wsaData;
        SOCKET  m_socket;
#else
        int     m_socket;
#endif
        struct addrinfo *p_server_addr, *p, hints;

        void initialize_socket();
        void connection_thread();
    };

}

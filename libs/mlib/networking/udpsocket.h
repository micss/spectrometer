#pragma once

#ifdef MLIBWIN
#include <winsock2.h>               // Winsock Library v2.0
#include <ws2tcpip.h>               // getaddrinfo
#pragma comment(lib,"ws2_32.lib")
#else
#include <stdio.h>
#include <string.h>         // memset()
#include <unistd.h>         // close()
#include <arpa/inet.h>
#include <sys/socket.h>
#include <ifaddrs.h>        // getifaddrs()
#endif

#include <string>

#include "foundation/defines.h"

namespace mlib {

    class UDPSocket
    {
    public:
        UDPSocket();
        ~UDPSocket();
        // methods
        int start_listening(int port);
        //int read(char* buffer, int num_bytes = UDP_MAX_PACKET_SIZE);
        std::string read_message();
        int write(const char* buffer, int num_bytes, std::string ip, int port);
        int broadcast(const char* buffer, int num_bytes, int port);

    private:
        int m_socket;
        int m_local_port;

        struct sockaddr_in local_addr;

        int initialize_socket();
        int close_socket();
    };
}

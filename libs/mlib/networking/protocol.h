#pragma once

#ifdef MLIBWIN
#include <winsock2.h>               // htonl()
#pragma comment(lib,"ws2_32.lib")
#else
#include <string.h>                 // memset() strcpy()        //TODO: remove
#include <unistd.h>                 // close()                  //TODO: remove
#include <arpa/inet.h>              // htonl()
#endif

#include <stdint.h>                 // uint8_t
#include <string>
#include <vector>

#include "networking/udpsocket.h"           // TODO: replace with network_interface

#define PROTOCOL_PREAMBLE           0x55CC  // message preamble
#define PROTOCOL_PREAMBLE_LENGTH    2       // [bytes] length of message preamble
#define PROTOCOL_SIZE_LENGHT        4       // [bytes] lengyh of the size field
#define PROTOCOL_CRC_LENGTH         2       // [bytes] length of CRC field          //TODO: this can be variable lenght if specified in header structure


namespace mlib {

    class Protocol
    {
    public:

        // generate a formatted message
        static std::string encode_message(int message_id, std::string payload);

        // send message on a given socket
        static int send_message(UDPSocket* socket, std::string remote_ip, int remote_port, int message_id, std::string payload);
        static int send_broadcast_message(UDPSocket* socket, int remote_port, int message_id, std::string payload);

        // extract message from a given socket
        static int receive_message(UDPSocket* socket, std::string &buffer); // blocking call

        // extract info from message
        static int get_project_id(const char* datagram);
        static int get_node_id(const char* datagram);
        static int get_message_id(const char* datagram);
        static const char* get_payload_pointer(const char* datagram);

        // setters
        static int set_project_id(int project_id);
        static int set_node_id(int node_id);

        // getters
        static int overhead();

        // helpers

        static int serialize(std::string &buffer, uint8_t &value);
        static int serialize(std::string &buffer, uint16_t &value);
        static int serialize(std::string &buffer, uint32_t &value);
        static int serialize(std::string &buffer, float &value);
        static int serialize(std::string &buffer, double &value);
        
        static int deserialize(const char* offset, uint8_t &value);
        static int deserialize(const char* offset, uint16_t &value);
        static int deserialize(const char* offset, uint32_t &value);
        static int deserialize(const char* offset, float &value);
        static int deserialize(const char* offset, double &value);

    private:

        static uint16_t version;        // Protocol version
        static int      project_id;     // Project ID
        static int      node_id;        // Node ID
    };
}

#include "networking/beacon.h"
#include "networking/protocol.h"

#include <iostream> // TODO: debug remove
#include <chrono>
#include <sstream>
#include <random>

//using clock = std::chrono::system_clock;
//using seconds = std::chrono::seconds;

namespace mlib {

    Beacon::Beacon()
    {
        // initialize modules
        p_udp_socket   = new UDPSocket();

        // initialize memory
        m_discoverable = true;
        m_listening    = true;

        // generate random beacon_id
        m_local_node.beacon_id = generate_beacon_id();
        std::cout << "BEACON> Hello! I am node [" << m_local_node.beacon_id << "]" << std::endl;

        // get local node info from network interface
        // TODO .. network_utils: get_local_ip(s) get_hostname()

        // get local node properties from configuration files
        // TODO .. use sunshine parser

        // start listening on Beacon port
        p_listener_thread  = new std::thread(&Beacon::listener, this);
        p_heartbeat_thread = new std::thread(&Beacon::heartbeat, this);
    }

    Beacon::~Beacon()
    {
        m_listening = false;
        m_discoverable = false;

        // close socket (unlock listener and heartbeat threads)
        delete p_udp_socket;

        // wait for threads to terminate nicely
        if(p_listener_thread) p_listener_thread->join();
        if(p_heartbeat_thread) p_heartbeat_thread->join();

        // clean up memory allocations
        // TODO: check if any
    }

    //
    //  MAKE DISCOVERABLE
    //  if true:  heartbeat thread is started, automatic responses to REQUEST messages.
    //  if false: heartbeat thread is stopped, REQUEST messages are ignored, beacon will
    //            keep listening to remote nodes and can still be manually triggered.
    //
    void Beacon::make_discoverable(bool discoverable)
    {
        if(discoverable == m_discoverable) return;

        m_discoverable = discoverable;

        if(m_discoverable == true) {
            p_heartbeat_thread = new std::thread(&Beacon::heartbeat, this);
        }
    }


    //
    //  SET LOCAL NODE PROPERTY
    //  save/edit a key-value property for local node
    //
    void Beacon::set_node_property(std::string key, std::string value)
    {
        // TODO: add property to m_node.property_list
    }

    void Beacon::set_node_property(std::string key, int value)
    {
        // TODO: int to string and then call preavious method
    }

    //
    //  TRIGGER
    //  send broadcast message to all other nodes with local node info
    //
    void Beacon::trigger()
    {

    }

    //
    //  LISTENER
    //  wait for identification requests from other nodes and reply point to point
    //
    void Beacon::listener()
    {
        int rc = p_udp_socket->start_listening(BEACON_DEFAULT_PORT);
        if (rc == RC_ERROR) std::cout << "BEACON> Error binding port" << std::endl;
        if (rc == RC_OK) std::cout << "BEACON> started listening" << std::endl;

        while(m_listening)
        {
            std::string message;

            // wait for incoming messages in mlib protocol format
            int rc = Protocol::receive_message(p_udp_socket, message);

            if(rc == RC_ERROR) {
                std::cout << "BEACON> ERROR receiveing incoming UDP message" << std::endl;      // TODO: remove this, log error
                continue;
            }

            int message_id = Protocol::get_message_id(message.data());

            // TODO: call decode_message?
            switch(message_id)
            {
            case MID_BEACON_REQUEST:
                std::cout << "BEACON> received a REQUEST for identification from a remote node" << std::endl;
                // TODO: send_identification(IP); // inside of the method check if discoverable
                break;
            case MID_BEACON_IDENTIFICATION:
                std::cout << "BEACON> received a REQUEST for identification from a remote node" << std::endl;
                // TODO: update_node_list(t_node); // add node if new, update info if exists
                break;
            case MID_BEACON_HEARTBEAT:
                {
                std::string beacon_id( Protocol::get_payload_pointer(message.data()) );
                update_last_seen(beacon_id);
                }
                break;
            default:
                break;
            }
        }
    }


    //
    //  HEARTBEAT
    //  Every N seconds sends an heartbeat broadcast message with just the beacon ID.
    //  Exits on m_discoverable false.
    //
    void Beacon::heartbeat()
    {
        std::string message = m_local_node.beacon_id;

        while(m_discoverable)
        {
            // send out heartbeat message
            Protocol::send_broadcast_message(p_udp_socket, BEACON_DEFAULT_PORT, MID_BEACON_HEARTBEAT, message);

            // clear offline nodes from the list (older than 3x Heartbeat_timout)
            // TODO

            std::this_thread::sleep_for(std::chrono::milliseconds(BEACON_TIMEOUT_HEARTBEAT));
        }
    }

    //
    //  GENERATE BEACON ID
    //  random sequence of 6 hex characters [16M combinations]
    //
    std::string Beacon::generate_beacon_id()
    {
        std::stringstream beacon_id;

        std::random_device rd;                          // obtain a random number from hardware
        std::mt19937 eng(rd());                         // seed the generator
        std::uniform_int_distribution<> distr(0, 15);   // define the range

        for(int n=0; n<BEACON_ID_LENGTH; ++n) {
            beacon_id << std::hex << distr(eng);
        }

        return beacon_id.str();
    }

    //
    //  UPDATE LAST SEEN TIMESTAMPS
    //
    void Beacon::update_last_seen(std::string beacon_id)
    {
        if(beacon_id == m_local_node.beacon_id) return;

        std::cout << "BEACON> received heartbeat from remote node [" << beacon_id << "]" << std::endl;

        // TODO: find node in the list, update last seen with local time
        // TODO: handle nodes sending heartbeats on 2 network interfaces
    }

}

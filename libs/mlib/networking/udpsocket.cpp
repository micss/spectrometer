#include "networking/udpsocket.h"

#include <iostream> //  TODO: remove debug

#ifdef MLIBWIN
#define CLOSE_SOCKET(S) closesocket((S));
#else
#define CLOSE_SOCKET(S) close((S));
#define SOCKET_ERROR    -1
#define INVALID_SOCKET  -1
#endif

// TODO: make it threa save with mutexes in read/write if sharing the same remote_addr

namespace mlib {

    UDPSocket::UDPSocket()
    {
        // initialize memory
        m_socket = INVALID_SOCKET;
        m_local_port = 0;
    }

    UDPSocket::~UDPSocket()
    {
        close_socket();
    }

    //
    //  START LISTENING
    //  bind the socket to a port for reading incoming packets, creates the socket is id not existing.
    //  Close and reapoen the socket if already listening on a different port
    //
    int UDPSocket::start_listening(int port)
    {
        // validate port number
        if((port < 0) || (port > 65536) ) return RC_ERROR;  // TODO: use range 1024-5000

        // initialize socket if it does not exists
        if(initialize_socket() == SOCKET_ERROR) return RC_ERROR;

        // if socket already binded on a different port, close and reinitialize socket. Also for same port number
        if(m_local_port != 0)
        {
            close_socket();
            if(initialize_socket() == SOCKET_ERROR) return RC_ERROR;
        }

        // bind port
        local_addr.sin_port = htons(port);
        if( bind(m_socket, (struct sockaddr*)&local_addr, sizeof(local_addr) ) == SOCKET_ERROR)
        {
            // TODO: log verbose error
            return RC_ERROR;
        }

        m_local_port = port;
        return RC_OK;
    }

    //
    //  READ
    //
    /*
    int UDPSocket::read(char* buffer, int num_bytes)    // TODO: add IP and PORT of the inming message as reference in arguments?
    {
        // return error if socket is not binded to any port
        if(m_local_port == 0) return RC_ERROR;

        // initialize socket if it does not exists alrready
        if(initialize_socket() == SOCKET_ERROR) return RC_ERROR;

        // read from sockets, returns -1 on error
        struct sockaddr_in remote_addr;
        socklen_t slen = sizeof(remote_addr);

        int received_bytes = 0;
        while(received_bytes < num_bytes)
        {
            int rc = recvfrom(m_socket, buffer, (size_t)(num_bytes - received_bytes), 0, (struct sockaddr *) &remote_addr, &slen);     // TODO, try NULL NULL is info about incoming IP and port not needed

            std::cout << "UDPSocket> received " << rc << " bytes out of " << num_bytes - received_bytes << std::endl;

            if(received_bytes < 0)
            {
                std::cout << "UDPSocket> Error reading from socket" << std::endl;
                // TODO: log error
                // TODO: if socket is closed, port should be re-binded here
                return RC_ERROR;
            }

            received_bytes += rc;
        }

        return received_bytes;
    }
    */

    //
    //  READ
    //
    std::string UDPSocket::read_message()    // TODO: add IP and PORT of the inming message as reference in arguments?
    {
        char buffer[65536];     // TODO: avoid continuous re-allocation of this temp buffer! set global + mutex
        std::string message;

        // return error if socket is not binded to any port
        if(m_local_port == 0) return message;

        // initialize socket if it does not exists alrready
        if(initialize_socket() == SOCKET_ERROR) return message;

        // read incoming message into temp buffer
        int received_bytes = recvfrom(m_socket, buffer, 65536, 0, NULL, NULL);     // TODO, try NULL NULL is info about incoming IP and port not needed

        if(received_bytes <= 0)
        {
            std::cout << "UDPSocket> Error reading from socket" << std::endl;
        }
        else
        {
            // copy buffer to string
            message.insert(message.begin(), buffer, buffer + received_bytes);
        }

        return message;
    }

    //
    //  WRITE
    //
    int UDPSocket::write(const char* buffer, int num_bytes, std::string ip, int port)
    {
        // set remote                               // TODO: just rewrite how IP/PORT of the remote pc are set
        struct sockaddr_in remote_addr;
        memset((char *) &remote_addr, 0, sizeof(remote_addr));
        remote_addr.sin_family = AF_INET;
        remote_addr.sin_port = htons(port);
        if (inet_aton(ip.c_str(), &remote_addr.sin_addr) == 0) {
            fprintf(stderr, "inet_aton() failed\n");
            return RC_ERROR;
        }

        // initialize socket if it does not exists alrready
        if(initialize_socket() == SOCKET_ERROR) return RC_ERROR;

        // send data, return -1 on error
        socklen_t slen = sizeof(remote_addr);
        int sent_bytes = sendto(m_socket, buffer, num_bytes, 0, (struct sockaddr*) &remote_addr, slen);

        if( sent_bytes == SOCKET_ERROR) {
            // TODO: log error
        }

        return sent_bytes;
    }

    //
    //  BROADCAST
    //  Send the message to the broadcast IP address of all the availables network interfaces
    //
    int UDPSocket::broadcast(const char* buffer, int num_bytes, int port)
    {

        // WINDOWS: GetAdaptersInfo() or GetAdaptersAddresses()

        struct ifaddrs *ifap, *ifa;
        struct in_addr subnet_mask, local_addr, broadcast_addr;

        // loop through all network interfaces
        getifaddrs(&ifap);
        for (ifa = ifap; ifa; ifa = ifa->ifa_next)
        {
            if (ifa->ifa_addr->sa_family == AF_INET)
            {
                // read local ip address and subnet mask of current network interface
                subnet_mask = ((struct sockaddr_in *) ifa->ifa_netmask)->sin_addr;
                local_addr  = ((struct sockaddr_in *) ifa->ifa_addr)->sin_addr;

                // calculate broadcast ip address
                broadcast_addr.s_addr = local_addr.s_addr | ~subnet_mask.s_addr;
                std::string broadcast_ip = std::string(inet_ntoa(broadcast_addr));

                // send broadcast message
                int sent_bytes = write(buffer, num_bytes, broadcast_ip, port);

                // TODO: re-send remaining bytes if sent_bytes < num_bytes, skip on 0
            }
        }

        freeifaddrs(ifap);
        return RC_OK;
    }

    //
    //  INITIALIZE SOCKET
    //  Create an UDP socket if it does not exists.
    //  Returns -1 on error, socket number otherwise.
    //
    int UDPSocket::initialize_socket()
    {
        if(m_socket == INVALID_SOCKET) {

            // create socket
            m_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

            // give socket broadcast permissions
            int broadcast=1;
            if (setsockopt(m_socket, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof broadcast) == SOCKET_ERROR) {
                perror("[UDP SOCKET] setsockopt (SO_BROADCAST)");
                return RC_ERROR;    // TODO: check the return value of this method wherever it is called
            }

            // make socket reusable
            int reusable=1;
            if (setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR, &reusable, sizeof(reusable)) == SOCKET_ERROR) {
                perror("[UDP SOCKET] setsockopt (SO_REUSEADDR)");
                // CLOSE_SOCKET(m_server_socket); // TODO: close on error?
                return RC_ERROR;    // TODO: check the return value of this method wherever it is called
            }

            // initialize socket addr (for binding later)
            memset((char *) &local_addr, 0, sizeof(local_addr));
            local_addr.sin_family = AF_INET;
            local_addr.sin_addr.s_addr = htonl(INADDR_ANY);         // listen to any IP addresses
        }

        if(m_socket == INVALID_SOCKET) {
            // TODO: log verbose error
        }

        return m_socket;
    }

    //
    //  CLOSE SOCKET
    //
    int UDPSocket::close_socket()
    {
        if (m_socket != INVALID_SOCKET)
            CLOSE_SOCKET(m_socket);

        m_socket = INVALID_SOCKET;
        m_local_port = 0;

        return RC_OK;
    }
}

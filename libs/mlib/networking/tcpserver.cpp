#include "networking/tcpserver.h"

#include <iostream> // DEBUG, TODO: remove this

#define RECEIVE_BUFFER_SIZE                 2048    // [bytes] size of the recv() temp buffer
#define MAX_NUM_CLIENTS                     10
#define TCPSERVER_STATEMACHINE_INTERVAL     100     //[ms] time interval between execution of state machine operations

#ifdef MLIBWIN
#define CLOSE_SOCKET(S) closesocket((S));
#else
#define CLOSE_SOCKET(S) close((S));
#define SOCKET_ERROR    -1
#define INVALID_SOCKET  -1
#endif

namespace mlib {

    enum TCPServerState { TCPSERVER_INITIALIZING, TCPSERVER_READY, TCPSERVER_LISTENING, TCPSERVER_ERROR,  TCPSERVER_TERMINATING, TCPSERVER_NUM_STATES };
    const char* tcpserver_state_name[] = { "TCPSERVER_INITIALIZING", "TCPSERVER_READY", "TCPSERVER_LISTENING", "TCPSERVER_ERROR", "TCPSERVER_TERMINATING" };

    TCPServer::TCPServer()
    {
        // initialize memory
        m_state = TCPSERVER_INITIALIZING;
        running = true;
        on_enter = true;
        m_max_num_clients = MAX_NUM_CLIENTS;
        callback_client_connected = nullptr;

        // spawn main thread
        p_main_thread = new std::thread(&TCPServer::main_loop, this);
    }

    TCPServer::~TCPServer() 
    {
        change_state(TCPSERVER_TERMINATING);    // receiver thread will exit if state is no longet LISTENING
        running = false;

        // wait for threads to terminate nicely
        if (p_receiver_thread != nullptr) p_receiver_thread->join();
        p_main_thread->join();

        if (m_server_socket != INVALID_SOCKET) {    // TODO: remove this check?
            CLOSE_SOCKET(m_server_socket);
        }

#ifdef MLIBWIN
        WSACleanup();
#endif
    }


    //
    //  REGISTER NEW CLIENT CALLBACKS
    //
    void TCPServer::register_callback_client_connected(std::function<void(int)> callback)
    {
        callback_client_connected = callback; // TODO: add to vector of callbacks
    }

    //
    //  START ACCEPTING CONNECTIONS
    //  store local port and initiate listening procedure. If the max number clients to listen to
    //  is not specified, the default MAX_NUM_CLIENTS is used.
    //
    int TCPServer::start_accepting_connections(std::string port, unsigned int max_num_clients /* = 0*/)
    {
        if (m_state != TCPSERVER_READY) {
            printf("TCPSERVER> ERROR: server is not in READY state \n");
            return RC_ERROR;
        }

        m_local_port = port;
        m_max_num_clients = max_num_clients;

        if (max_num_clients == 0) { max_num_clients = MAX_NUM_CLIENTS; }

        change_state(TCPSERVER_LISTENING);
        return RC_OK;
    }

    //
    //  READ
    //  Blocking call. Try to read 'num_bytes' from 'socket' and copy them in 'buffer'.
    //  Returns number of bytes received, 0 on connection closed, <0 on recv() error.
    //
    int TCPServer::read(int socket, char* buffer, int num_bytes)
    {
        // check if socket is in the list of active connections
        if (!is_socket_valid(socket)) return -1;

        int bytes_received = recv(socket, buffer, num_bytes, 0);

        if (bytes_received == 0) {
            printf("TCPServer> Connection close by peer\n");
            // TODO: close socket, remove socket from list and fire disconnect callback
        }

        if (bytes_received < 0) {
            printf("TCPServer> Recv() failed\n");
            // TODO check socket error
            // ..
            // TODO remove socket from list of active connections and fire disconnect callback
            // ..
        }

        return bytes_received;
    }

    //
    //  WRITE
    //  blocking call. Try to write 'num_bytes' from buffer to socket, can use more than one send().
    //  Returns RC_OK if it was possible to sent all the data, RC_ERROR otherwise.
    //
    int TCPServer::write(int socket, char* buffer, int num_bytes)
    {
        // check if socket is in the list of active connections
        if (!is_socket_valid(socket)) return -1;

        // TODO lock socket_N mutex      // TODO: use structure to store socketid, client info and write lock
        // ..

        int rc, bytes_sent = 0;

        while (bytes_sent < num_bytes)
        {
            rc = send(socket, buffer+bytes_sent, num_bytes-bytes_sent, 0);

            if (rc == SOCKET_ERROR) {
                printf("TCPServer> send failed\n");
                
                // TODO check last socket error (both win/linux)
                // WSAGetLastError()/perror()  getLastSocketError(socket_id)
                
                // TODO: handle partial sends
                // ..
                
                return RC_ERROR;
            }

            bytes_sent += rc;
        }

        return RC_OK;
    }


    //
    //  MAIN LOOP
    //  State Machine
    //
    void TCPServer::main_loop()
    {
        while (running)
        {
            switch (m_state)
            {
            case TCPSERVER_INITIALIZING:
                state_initializing();
                break;
            case TCPSERVER_READY:
                state_ready();
                break;
            case TCPSERVER_LISTENING:
                state_listening();
                break;
            case TCPSERVER_TERMINATING:
                state_terminating();
                break;
            case TCPSERVER_ERROR:
                state_error();
                break;
            }

            std::this_thread::sleep_for(std::chrono::milliseconds(TCPSERVER_STATEMACHINE_INTERVAL));
        }
    }


    //
    //  STATE INITIALIZING
    //  on enter, try to initialize socket context and data structure, then go to READY state
    //
    void TCPServer::state_initializing()
    {
        if (m_state != TCPSERVER_INITIALIZING) return;

#ifdef MLIBWIN

        // WINDOWS PLATFORM
        int rc = WSAStartup(MAKEWORD(2, 0), &wsaData);
        if (rc != 0) {
            printf("WSAStartup failed \n");
            change_state(TCPSERVER_ERROR);
            return;
        }

        ZeroMemory(&hints, sizeof(hints));
#else
        // UNIX PLATFORM
        memset(&hints, 0, sizeof hints);
#endif
        hints.ai_family   = AF_INET;        // TODO: check AF_UNSPEC (working on linux but not on windows)
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_protocol = IPPROTO_TCP;
        hints.ai_flags    = AI_PASSIVE;     // use my IP

        change_state(TCPSERVER_READY);
    }

    //
    //  STATE READY
    //  the server is initialized but not listening for incoming connections
    //
    void TCPServer::state_ready()
    {
        if (m_state != TCPSERVER_READY) return;

        if (on_enter)
        {
            // TODO: check a flag to start listening automatically (after recovering from an error or to start listening at startup)
            // ...
            on_enter = false;
        }

        // do nothing, wait for a user command to start listening
    }

    //
    //  STATE LISTENING
    //  on enter, create a socket, bind it to a local port, spawn the listening thread
    //
    void TCPServer::state_listening()
    {
        if (m_state != TCPSERVER_LISTENING) return;

        if (on_enter)
        {

#ifdef MLIBWIN
            // WINDOWS PLATFORM     //TODO: loop over getaddrinfo() as in Unix implementation

            // get the address info
            if (getaddrinfo(NULL, m_local_port.data(), &hints, &res) != 0) {
                perror("getaddrinfo");
                m_state = TCPSERVER_ERROR;
                return;
            }

            // Create the server socket
            m_server_socket = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
            if (m_server_socket == INVALID_SOCKET) {
                perror("socket");
                WSACleanup();
                m_state = TCPSERVER_ERROR;
                return;
            }

            // Enable the socket to reuse the address
            int reuseaddr = 1; // true
            if (setsockopt(m_server_socket, SOL_SOCKET, SO_REUSEADDR, (const char *)&reuseaddr, sizeof(int)) == SOCKET_ERROR) {
                perror("setsockopt");
                WSACleanup();
                m_state = TCPSERVER_ERROR;
                return;
            }

            // Bind to the address
            if (bind(m_server_socket, res->ai_addr, (int)res->ai_addrlen) == SOCKET_ERROR) {
                perror("bind");
                WSACleanup();
                m_state = TCPSERVER_ERROR;
                return;
            }

            // listen
            if (listen(m_server_socket, m_max_num_clients) == SOCKET_ERROR) {
                perror("listen");
                WSACleanup();
                m_state = TCPSERVER_ERROR;
                return;
            }

            freeaddrinfo(res);
#else
            // UNIX PLATFORM
            struct addrinfo *servinfo, *p;
            int rc;

            if ((rc = getaddrinfo(NULL, m_local_port.data(), &hints, &servinfo)) != 0)
            {
                fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rc));
                change_state(TCPSERVER_ERROR);
                return;
            }

            // loop through all the results and bind to the first we can
            for(p = servinfo; p != NULL; p = p->ai_next)
            {
                if ((m_server_socket = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
                    perror("server: socket");
                    continue;
                }

                int yes = 1;
                if (setsockopt(m_server_socket, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
                    perror("setsockopt");
                    CLOSE_SOCKET(m_server_socket);
                    continue;
                }

                if (bind(m_server_socket, p->ai_addr, p->ai_addrlen) == -1) {
                    perror("server: bind");
                    CLOSE_SOCKET(m_server_socket);
                    continue;
                }

                break;
            }

            freeaddrinfo(servinfo);

            if (p == NULL)  {
                fprintf(stderr, "server: failed to bind\n");
                change_state(TCPSERVER_ERROR);
                return;
            }

            if (listen(m_server_socket, m_max_num_clients) == -1) {
                perror("listen");
                change_state(TCPSERVER_ERROR);
                return;
            }

            // reap all dead processes
            //sa.sa_handler = sigchld_handler;
            //sigemptyset(&sa.sa_mask);
            //sa.sa_flags = SA_RESTART;
            //if (sigaction(SIGCHLD, &sa, NULL) == -1) {
            //   perror("sigaction");
            //   exit(1);
            //}
#endif
            // spawn listener thread
            p_receiver_thread = new std::thread(&TCPServer::receiver, this);

            on_enter = false;
        }

        // TODO: monitor connections, implement keep alive
        // ...
    }

    //
    //  STATE TERMINATING
    //  refuse any new incoming connection, wait for all active connections to close nicely
    //  then reinitialize socket to get back to READY state
    //
    void TCPServer::state_terminating()
    {
        // terminate receiver thread
        CLOSE_SOCKET(m_server_socket);      // this unlocks the accept call in the receiver thread
        if(p_receiver_thread) p_receiver_thread->join();
        delete p_receiver_thread;

        // TODO: timeout if thread is not exiting
        // ..

        // iterate over connection list and close all active connections
        for (auto const& socket : m_socket_list) {
            CLOSE_SOCKET(socket);
        }
        m_socket_list.clear();

        // TODO: check that all objects are reset to ready state
        m_server_socket   = INVALID_SOCKET;
        p_receiver_thread = nullptr;

#ifdef MLIBWIN
        WSACleanup();
#endif

        change_state(TCPSERVER_INITIALIZING);
    }

    //
    //  STATE ERROR
    //  Try to recover from errors and get back to READY state
    //
    void TCPServer::state_error()
    {
        if (m_state != TCPSERVER_ERROR) return;
        
        // wait few seconds     // TODO: dynamic waiting time, make it longer if in a loop "initializing<->error" or "error<->error"
        std::this_thread::sleep_for(std::chrono::milliseconds(3000));

        // TODO: try to recover from error whenever possible without closing all connections
        // ..

        // if urecoverable error, reinitialize server (close all connections + reconfigure socket)
        change_state(TCPSERVER_TERMINATING);
    }


    //
    //  HELPER Change State
    //
    void TCPServer::change_state(int state)
    {
        // TODO: save m_state_prev
        m_state = state;
        on_enter = true;
    }

    //
    //  RECEIVER THREAD
    //  accept incoming connections, store them in a list, fire callbacks
    //
    void TCPServer::receiver()
    {
        // TODO: can receiver thread exit and be restarted during the same session without destroying the server? how this affect the connection ids in the logs?
        printf("TCPServer> [Receiver thread] Waiting for incoming connections on port %s\n", m_local_port.c_str());

        // TODO: put together the windows/linux implementations with common error handling

        while (m_state == TCPSERVER_LISTENING)
        {

#ifdef MLIBWIN
            // WINDOWS PLATFORM

            // Accept a client socket
            SOCKET client_socket = accept(m_server_socket, NULL, NULL);

            if (client_socket == INVALID_SOCKET)
            {
                printf("TCPSERVER> accept failed with error: %d\n", WSAGetLastError());
                
                // close all connections and reinitialize socket
                //change_state(TCPSERVER_TERMINATING);
                //break;
                continue;   // reinitialize the server on a single connection error?
            }

            // get client info ip/port
            // ..
#else
            // UNIX PLATFORM
            struct sockaddr_storage client_addr;

            socklen_t sin_size = sizeof client_addr;
            int client_socket = accept(m_server_socket, (struct sockaddr *)&client_addr, &sin_size);
            if (client_socket == -1) {
                perror("accept");
                continue;
            }

            // get client info ip/port
            // ..
#endif

            // add new client to socket list
            printf("TCPServer> ACCEPTED INCOMING CONNECTION\n");
            m_socket_list.push_back((int)client_socket);

            // fire callbacks
            if (callback_client_connected != nullptr) callback_client_connected((int)client_socket); // TODO, loop over vector of callbacks
        }

        printf("TCPServer> [Receiver thread] terminated\n");
    }

    //
    // GETTERS
    //
    bool TCPServer::ready()
    {
        return (m_state == TCPSERVER_READY) ? true : false;
    }

    //
    // HELPERS
    //
    bool TCPServer::is_socket_valid(int socket)
    {
        bool res = false;

        for (int &it : m_socket_list)
        {
            if( it == socket)
            {
                res = true;
                break;
            }
        }

        return res;
    }
}

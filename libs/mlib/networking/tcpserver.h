#pragma once

#ifdef MLIBWIN
#include <winsock2.h>               // Winsock Library v2.0
#include <ws2tcpip.h>               // getaddrinfo
#pragma comment(lib,"ws2_32.lib")
#else
#include <string.h>                 // memset() strcpy()
#include <unistd.h>                 // close()
#include <arpa/inet.h>              // struct sockaddr_in
#include <sys/types.h>              // struct addrinfo
#include <sys/socket.h>             // socket()
#include <sys/time.h>               // struct timeval
#include <netdb.h>
#include <fcntl.h>                  // fcntl() F_SETFL
//#include <stdio.h>
//#include <stdlib.h>
//#include <errno.h>
//#include <netinet/in.h>
#endif

#include <functional>
#include <thread>
#include <vector>

#include "foundation/defines.h"

namespace mlib {

    class TCPServer
    {
    public:
        TCPServer();
        ~TCPServer();
        // methods
        void register_callback_client_connected(std::function<void(int)> callback);
        int  start_accepting_connections(std::string port, unsigned int max_num_clients = 0);
        //int stop_accepting_connections();
        //int close_all_connections();
        int  read(int socket, char* buffer, int num_bytes);   // blocking call
        int  write(int socket, char* buffer, int num_bytes);  // blocking call
        // getters
        bool ready();

    private:
        int             m_state;
        std::thread*    p_main_thread;
        std::thread*    p_receiver_thread;
        bool            running;
        bool            on_enter;
        std::string     m_local_ip;
        std::string     m_local_port;
        int             m_max_num_clients;

        std::vector<int> m_socket_list;
        std::function<void(int)> callback_client_connected;

#ifdef MLIBWIN
        WSADATA             wsaData;
        SOCKET              m_server_socket;
#else
        int                 m_server_socket;
#endif
        struct addrinfo *res, hints;

        void main_loop();
        void state_initializing();
        void state_ready();
        void state_listening();
        void state_terminating();
        void state_error();
        void change_state(int state);
        void receiver();

        bool is_socket_valid(int socket);
    };

}

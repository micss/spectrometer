#include "protocol.h"
#include "hashing/crc.h"

#include <iostream>     //TODO: debug, remove
#include <cstring>      // memcpy

namespace mlib {

    // initialize global variables with defaults
    int      Protocol::project_id   = 0;                    // TODO: read from configuration files
    int      Protocol::node_id      = 0;                    // TODO: read from configuration files

    // Protocol header [ID]
    struct t_header
    {
        uint8_t project_id;
        uint8_t node_id;
        uint8_t message_id;

        int serialize(std::string &buffer)
        {
            int bytes = 0; //buffer.size();
            bytes += Protocol::serialize(buffer, project_id);
            bytes += Protocol::serialize(buffer, node_id);
            bytes += Protocol::serialize(buffer, message_id);
            return bytes;
        }

        const char* parse(const char* offset)
        {
            offset += Protocol::deserialize(offset, project_id);
            offset += Protocol::deserialize(offset, node_id);
            offset += Protocol::deserialize(offset, message_id);
            return offset;
        }
    };



    //
    //  ENCODE MESSAGE
    //  Prepares a datagram as a std::string in the form PRE|SIZE|<ID|PAYLOAD>|CRC.
    //  Returns a copy of the datagram. It would be more efficient to return a pointer to the
    //  string, but if later vector is not deallocated by caller there will be memory leaks     // TODO: smart pointers?
    //
    std::string Protocol::encode_message(int message_id, std::string payload)                   // TODO: avoid insert at the beginning of the string
    {
        std::string datagram;

        // reserve space for message
        datagram.reserve(Protocol::overhead() + payload.size());

        // populate header structure
        t_header header;
        header.project_id = Protocol::project_id;
        header.node_id    = Protocol::node_id;
        header.message_id = message_id;

        // serialize header bytes and write them in datagram vector
        header.serialize(datagram);
        
        // append payload bytes
        datagram.insert(datagram.end(), payload.begin(), payload.end());

        // calculate and append CRC (2-bytes)
        uint16_t crc = CRC16::calculate(datagram.data(), datagram.size());
        Protocol::serialize(datagram, crc);

        // add size (4-bytes) at the front
        uint32_t size = (uint32_t)datagram.size();
        uint32_t nsize = htons(size);               //TODO: use a swap(var) method
        datagram.insert(datagram.begin(), (char*)&nsize, (char*)&nsize + sizeof(nsize));

        // add preamble (2-bytes) at the front
        uint16_t pre = PROTOCOL_PREAMBLE;
        uint16_t npre = htons(pre);                 //TODO: use a swap(var) method
        datagram.insert(datagram.begin(), (char*)&npre, (char*)&npre + sizeof(npre));

        return datagram;
    }

    //
    //  SEND MESSAGE (UDP)
    //
    int Protocol::send_message(UDPSocket* socket, std::string remote_ip, int remote_port, int message_id, std::string payload)
    {
        std::string message = encode_message(message_id, payload);  // TODO: optimize this, too much copying memory around

        // check message max size
        if(message.size() > 65536) return -1;                       // TODO: ERROR_PROTOCOL_.. create an ENUM for protocol errors! MOVE CHECK TO UDP SOCKET CLASS

        // send message
        int sent_bytes = socket->write(message.data(), message.size(), remote_ip, remote_port);

        return sent_bytes;
    }

    //
    //  SEND BROADCAST MESSAGE
    //
    int Protocol::send_broadcast_message(UDPSocket* socket, int remote_port, int message_id, std::string payload)
    {
        std::string message = encode_message(message_id, payload);

        // check message max size
        if(message.size() > 65536) return -1;                       // TODO: ERROR_PROTOCOL_.. create an ENUM for protocol errors! MOVE CHECK TO UDP SOCKET CLASS

        // send message
        int sent_bytes = socket->broadcast(message.data(), message.size(), remote_port);

        return sent_bytes;
    }


    //
    //  EXTRACT A MESSAGE FROM A SOCKET
    //  the entire datagram is received from UDP socket. skipped checks on preamble and size.
    //  only CRC is checked to validate message.
    //  Copy back in the buffer the relevant part of the message <ID|PAYLOAD>
    //  Return message size, RC_ERROR in case of error
    //
    int Protocol::receive_message(UDPSocket* socket, std::string &buffer)
    {
        std::string datagram = socket->read_message();

        if(datagram.empty()) {
            std::cout << "PROTOCOL> ERROR cannot retrieve UDP message" << std::endl;
            return RC_ERROR;
        }

        // extract CRC
        uint16_t ncrc = 0;
        std::memcpy(&ncrc, datagram.data() + datagram.size() - PROTOCOL_CRC_LENGTH, PROTOCOL_CRC_LENGTH);
        uint16_t hcrc = ntohs(ncrc);

        // extract message              // TODO: this is not optimized
        std::string message = datagram.substr(PROTOCOL_PREAMBLE_LENGTH + PROTOCOL_SIZE_LENGHT, datagram.size() - PROTOCOL_CRC_LENGTH - PROTOCOL_PREAMBLE_LENGTH - PROTOCOL_SIZE_LENGHT);

        // verify message integrity
        uint16_t crc = CRC16::calculate(message.data(), message.size());
        if(crc != hcrc) {
            std::cout << "PROTOCOL> ERROR: wrong CRC" << std::endl;
            return RC_ERROR;            // TODO: do not return error!! just start waiting for the next message, keep the caller blocked
        }

        // copy message for caller      // TODO: this is not optimized
        buffer = message;

        return RC_OK;
    }


    //
    //  GET PROJECT ID
    //  return -1 if information is not available in current protocol version
    //
    int Protocol::get_project_id(const char* datagram)
    {
        int res = -1;
        
        t_header header;
        header.parse(datagram);
        res = header.project_id;

        // validity check on sensor type ID: >0 & <MAX (in foundation constants)
        // ...

        return res;
    }

    //
    //  GET NODE ID
    //
    int Protocol::get_node_id(const char* datagram)
    {
        int res = -1;

        t_header header;
        header.parse(datagram);
        res = header.node_id;

        // validity check on sensor type ID: >0 & <MAX (in foundation constants)
        // ...

        return res;
    }

    //
    //  GET MESSAGE ID from datagram
    //
    int Protocol::get_message_id(const char* datagram)
    {
        int res = -1;

        t_header header;
        header.parse(datagram);
        res = header.message_id;

        // validity check on sensor type ID: >0 & <MAX (in foundation constants)
        // ...

        return res;
    }

    //
    //  GET PAYLOAD POINTER from datagram
    //  return NULL pointer if there is no payload in the message
    //
    const char* Protocol::get_payload_pointer(const char* datagram)
    {
        const char* res = nullptr;

        t_header header;
        res = header.parse(datagram);

        return res;
    }

    //
    // SETTERS
    //
    int Protocol::set_project_id(int project_id)
    {
        Protocol::project_id = project_id;
    }

    int Protocol::set_node_id(int node_id)
    {
        Protocol::node_id = node_id;
    }

    //
    //  CALCULATE PROTOCOL OVERHEAD
    //  Returns 0 on error, overhead bytes otherwise
    //
    int Protocol::overhead()
    {
        int header_length = sizeof(t_header);

        int overhead_length = PROTOCOL_PREAMBLE_LENGTH + PROTOCOL_SIZE_LENGHT + header_length + PROTOCOL_CRC_LENGTH;

        return overhead_length;
    }

    //
    //  HELPERS: SERIALIZE
    //  Return number of bytes written
    //
    int Protocol::serialize(std::string &buffer, uint8_t &value)
    {
        buffer.push_back((char)value);
        return sizeof(value);
    }
    int Protocol::serialize(std::string &buffer, uint16_t &value)
    {
        uint16_t temp = htons(value);   // TODO: if is_big_endian() swap_bytes()
        buffer.insert(buffer.end(), (char*)&temp, (char*)&temp + sizeof(value));
        return sizeof(value);
    }
    int Protocol::serialize(std::string &buffer, uint32_t &value)
    {
        uint32_t temp = htonl(value);   // TODO: if is_big_endian() swap_bytes()
        buffer.insert(buffer.end(), (char*)&temp, (char*)&temp + sizeof(value));
        return sizeof(value);
    }
    int Protocol::serialize(std::string &buffer, float &value)
    {
        float temp = value;
        // TODO: if is_big_endian() swap_bytes()
        // TODO: how long is a float on every architecture?
        buffer.insert(buffer.end(), (char*)&temp, (char*)&temp + sizeof(value));
        return sizeof(value);
    }
    int Protocol::serialize(std::string &buffer, double &value)
    {
        double temp = value;
        // TODO: if is_big_endian() swap_bytes()
        // TODO: how long is a float on every architecture?
        buffer.insert(buffer.end(), (char*)&temp, (char*)&temp + sizeof(value));
        return sizeof(value);
    }

    //
    //  HELPERS: DESERIALIZE
    //  Return number of bytes read
    //
    int Protocol::deserialize(const char* offset, uint8_t &value)
    {
        value = *(uint8_t*)offset;
        return sizeof(value);
    }
    int Protocol::deserialize(const char* offset, uint16_t &value)
    {
        value = ntohs( *(uint16_t*)offset );
        return sizeof(value);
    }
    int Protocol::deserialize(const char* offset, uint32_t &value)
    {
        value = ntohl(*(uint32_t*)offset);
        return sizeof(value);
    }
    int Protocol::deserialize(const char* offset, float &value)
    {
        // TODO: if is_big_endian() swap_bytes()
        // TODO: how long is a float on every architecture?
        memcpy(&value, offset, sizeof(value));
        return sizeof(value);
    }
    int Protocol::deserialize(const char* offset, double &value)
    {
        // TODO: if is_big_endian() swap_bytes()
        // TODO: how long is a float on every architecture?
        memcpy(&value, offset, sizeof(value));
        return sizeof(value);
    }

}

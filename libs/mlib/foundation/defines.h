#pragma once

// reserved Message IDs for system messages [ 0x10 - 0x40 ]

#define MID_BEACON_REQUEST          0x10
#define MID_BEACON_IDENTIFICATION   0x11
#define MID_BEACON_HEARTBEAT        0x12

// enums

namespace mlib {

    enum returnCode   { RC_ERROR, RC_OK };

}

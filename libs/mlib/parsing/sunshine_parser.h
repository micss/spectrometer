#pragma once

#include <string>
#include <map>

#include "foundation/defines.h"

#define SUNSHINE_OUTER_DELIMITER ';'
#define SUNSHINE_INNER_DELIMITER '='

namespace mlib {

    class SunshineParser
    {
    public:
        SunshineParser();
        ~SunshineParser();
        // methods
        int parse(std::string source);
        int parse_file(std::string filename);
        int lookup(std::string key, std::string &value);
        int lookup(std::string key, int &value);
        //void clear() { m_dictionary.clear(); }
        // getters
        //std::map<std::string, std::string> dictionary() { return m_dictionary; }
        //std::string export_dictionary();

    private:
        std::map<std::string, std::string> m_dictionary;

    };

}

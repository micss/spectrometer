#include "parsing/sunshine_parser.h"

#include <iostream>
#include <sstream>
#include <regex>

namespace mlib {

    SunshineParser::SunshineParser()
    {
    }

    SunshineParser::~SunshineParser()
    {
    }

    //
    //  PARSE
    //  load internal dictionary
    //
    int SunshineParser::parse(std::string source)
    {
        // remove endlines
        std::replace(source.begin(), source.end(), '\n', SUNSHINE_OUTER_DELIMITER);
        std::replace(source.begin(), source.end(), '\r', SUNSHINE_OUTER_DELIMITER);

        // loop over tokens
        std::istringstream ssi(source);
        std::string token;

        while(std::getline(ssi, token, SUNSHINE_OUTER_DELIMITER))
        {
            // split token
            std::istringstream sst(token);
            std::string key;
            std::string value;

            std::getline(sst, key, SUNSHINE_INNER_DELIMITER);
            std::getline(sst, value, SUNSHINE_INNER_DELIMITER);

            // trim spaces
            key = std::regex_replace(key, std::regex("^ +| +$|"), "$1");        // std::regex("^ +| +$|( ) +") to trim extra spaces
            value = std::regex_replace(value, std::regex("^ +| +$"), "$1");

            // skip incomplete/empty tokens
            if( key.empty() || value.empty() ) continue;

            // key to uppercase
            for (auto & c: key) c = toupper(c);

            // update value if exists otherwise add to dictionary
            m_dictionary[key]=value;
        }
    }

    //
    //  PARSE file
    //  read file content and call parse method
    //
    int SunshineParser::parse_file(std::string filename)
    {
        // TODO: to be implemented
        return RC_ERROR;
    }

    //
    //  LOOKUP TEXT value
    //
    int SunshineParser::lookup(std::string key, std::string &value)
    {
        auto it = m_dictionary.find(key);

        if (it != m_dictionary.end()) {
            value = it->second;
            return RC_OK;
        }

        return RC_ERROR;
    }

    //
    //  LOOKUP INTEGER value
    //
    int SunshineParser::lookup(std::string key, int &value)
    {
        auto it = m_dictionary.find(key);

        if (it != m_dictionary.end())
        {
            std::string str_value = it->second;

            // TODO: check with regex if numbers only
            value = std::stoi(str_value);

            return RC_OK;
        }

        return RC_ERROR;
    }

}

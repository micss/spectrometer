#include "thirdparty/wiringPi/wiringPi.h"
#include "gpio/gpio_manager.h"
#include <iostream> //DEBUG

namespace mraspi {

    GPIOManager::GPIOManager()
    {
        // init memory
        // ..
    }

    GPIOManager::~GPIOManager()
    {
        // cleanup
        // ..
    }

    void GPIOManager::initialize()
    {
        wiringPiSetup();
        pinMode (26, OUTPUT);
    }

    void GPIOManager::toggle_led()
    {
        static int counter = 0;

        counter++;

        if(counter%2 == 0)
            digitalWrite (26, HIGH);
        else
            digitalWrite (26, LOW);

        std::cout << "blinking on gpio 26" << std::endl;
    }
}

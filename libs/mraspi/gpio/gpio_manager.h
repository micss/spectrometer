#pragma once

namespace mraspi {

    class GPIOManager
    {
    public:
        GPIOManager();
        ~GPIOManager();

        // debug ----------------
        void initialize();
        void toggle_led();
        // debug ----------------
    };

}

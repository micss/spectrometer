#include "controller.h"

Controller::Controller()
{
    // initialize controller modules
    p_sensor = new SensorInterface();
}

Controller::~Controller()
{
    // call destructors of all modules
    delete p_sensor;
}

//
// CONNECT
//
void Controller::connect(std::string ip_address, std::string port)
{
    // ask the sensor interface to connect to a specific IP/port (when not using auto discovery)
    p_sensor->connect(ip_address, port);
}

//
// DISCONNECT
//
void Controller::disconnect()
{
    // cose connection between the sensor interface and the sensor device
    p_sensor->disconnect();
}

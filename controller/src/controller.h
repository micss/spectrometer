#pragma once

#include "sensor_interface.h"

class Controller
{
public:
    Controller();
    ~Controller();
    // methods
    void connect(std::string ip_address, std::string port);
    void disconnect();
    // getters
    SensorInterface* sensor() { return p_sensor; }

private:
    SensorInterface* p_sensor;
};

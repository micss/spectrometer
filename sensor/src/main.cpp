#include "sensor.h"

#include <chrono>
#include <thread>

int main(int argc, char *argv[])
{
    // Create sensor object
    Sensor* sensor = new Sensor();

    // Keep the process alive as long as the sensor threads are running.
    // When the sensor is shutdown via TCP command, close the process.
    while (sensor->is_running()) {
        std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    }
    // sensor->join()  // TODO: call a join on the main thread instead

    delete sensor;
    return 0;
}

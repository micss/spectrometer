#pragma once

#include "networking/tcpclient.h"
#include "networking/udpsocket.h"

enum SensorIFState { SENSORIF_DISCONNECTED, SENSORIF_CONNECTING, SENSORIF_CONNECTED };

class SensorInterface
{
public:
    SensorInterface();
    ~SensorInterface();
    // methods
    void connect(std::string ip_address, std::string port);
    void disconnect();
    // getters
    int state() { return m_state; }

private:
    std::thread*    p_main_thread;
    std::thread*    p_listener_thread;
    std::mutex      m_mutex;
    mlib::TCPClient* p_tcp_client;
    mlib::UDPSocket* p_udp_socket;
    std::string     m_sensor_ip;
    std::string     m_sensor_port;
    int             m_state;
    bool            running;
    bool            on_enter;

    void mainLoop();
    void listener();
    void change_state(int state);
    void state_disconnected();
    void state_connecting();
    void state_connected();
};

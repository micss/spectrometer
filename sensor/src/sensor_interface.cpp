#include "sensor_interface.h"

#define SENSORIF_STATEMACHINE_INTERVAL    100  //[ms] time interval between ececution of state machine operations

using namespace mlib;

SensorInterface::SensorInterface()
{
    // initialize modules
    p_tcp_client = new TCPClient();
    p_udp_socket = new UDPSocket();

    // DEBUG ---
    char buffer3[3] = {'B','R','C'};
    p_udp_socket->broadcast(buffer3, 3, 10549);
    // DEBUG ---

    // initialize memory
    running  = true;
    on_enter = true;
    m_state  = SENSORIF_DISCONNECTED;

    // spawn thread
    p_main_thread = new std::thread(&SensorInterface::mainLoop, this);
    p_listener_thread = new std::thread(&SensorInterface::listener, this);
}

SensorInterface::~SensorInterface()
{
    running = false;

    // wait for threads to terminate nicely
    p_main_thread->join();
    p_listener_thread->join();

    // close TCP connection and destroy socket
    delete p_tcp_client;
    delete p_udp_socket;
}

//
//  MAIN LOOP
//  State Machine
//
void SensorInterface::mainLoop()
{
    while (running)
    {
        switch (m_state)
        {
        case SENSORIF_DISCONNECTED:
            state_disconnected();
            break;
        case SENSORIF_CONNECTING:
            state_connecting();
            break;
        case SENSORIF_CONNECTED:
            state_connected();
            break;
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(SENSORIF_STATEMACHINE_INTERVAL));
    }
}

//
//  LISTENER THREAD
//  Parse incoming data
//
void SensorInterface::listener()
{
    while (running)
    {
        // read from socket (blocking call)
        // ..

        // get all the message (read untill data in socket)
        // ..

        // extract packet header
        // ..

        // parse the following bytes accordingly
        // ..

        // REMOVE THE SLEEP when method fully implemented
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
}

//
//  CHANGE CURRENT STATE
//
void SensorInterface::change_state(int state)
{
    //TODO: save previous state
    m_state  = state;
    on_enter = true;
}

//
//  STATE DISCONNECTED
//
void SensorInterface::state_disconnected()
{
    std::lock_guard<std::mutex> lock(m_mutex);

    // Double check current state
    if (m_state != SENSORIF_DISCONNECTED) return;

    if (on_enter) {
        // clear some memory
        // TODO ...
        on_enter = false;
    }

    // do something, try to auto-reconnect for 3 attempts
    // TODO ..
}

//
//  STATE CONNECTING
//
void SensorInterface::state_connecting()
{
    std::lock_guard<std::mutex> lock(m_mutex);

    // Double check current state
    if (m_state != SENSORIF_CONNECTING) return;

    if (on_enter) {
        // try to connect
        p_tcp_client->open_connection(m_sensor_ip, m_sensor_port);
        on_enter = false;
    }

    // Check the connection state
    if ( p_tcp_client->connected() )
    {
        change_state(SENSORIF_CONNECTED);
    }

    if ( p_tcp_client->disconnected() )
    {
        // p_socket->get_error_message()
        // ..
        change_state(SENSORIF_DISCONNECTED);
    }
}

//
//  STATE CONNECTED
//
void SensorInterface::state_connected()
{
    std::lock_guard<std::mutex> lock(m_mutex);

    // Double check current state
    if (m_state != SENSORIF_CONNECTED) return;

    // On entering the state request connection
    if (on_enter) {

        // DEBUG ---
        char buffer[3] = {'A','b','C'};
        p_tcp_client->write(buffer,3);
        // DEBUG ---


        // fire a (gui) callback function if registered
        // ..
        on_enter = false;
    }

    // check if the connection holds
    if ( p_tcp_client->disconnected() ) {
        change_state(SENSORIF_DISCONNECTED);
    }
}

//
// DO CONNECT
//
void SensorInterface::connect(std::string ip_address, std::string port)
{
    std::lock_guard<std::mutex> lock(m_mutex);

    // change board state: DISCONNECTED => CONNECTING
    if ( m_state == SENSORIF_DISCONNECTED )
    {
        m_sensor_ip = ip_address;
        m_sensor_port = port;
        change_state(SENSORIF_CONNECTING);
    }


    // TODO: handle case when alreasy trying to enstablish a connection
}

//
// DO DISCONNECT
//
void SensorInterface::disconnect()
{
    std::lock_guard<std::mutex> lock(m_mutex);

    if ( m_state == SENSORIF_CONNECTED )
    {
        p_tcp_client->close_connection();
        change_state(SENSORIF_DISCONNECTED);
    }
}


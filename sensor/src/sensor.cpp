#include "sensor.h"
#include <iostream> // DEBUG TODO remove

#define SENSOR_MAINLOOP_INTERVAL    200     // [ms] time interval between execution of state functions

using namespace mlib;

Sensor::Sensor()
{
    // initialize memory
    running     = true;

    // initialize modules
    p_tcp_server = new TCPServer();
    p_beacon = new Beacon();

    // spawn threads
    p_mainloop_thread = new std::thread(&Sensor::mainloop, this);
    p_listener_thread = nullptr;    // TODO: use a vector of threads
}

Sensor::~Sensor()
{
    running = false;

    // wait for threads to terminate nicely
    p_mainloop_thread->join();
    if(p_listener_thread) p_listener_thread->join();

    // close TCP connection and destroy socket
    delete p_tcp_server;
}

//
//  MANI LOOP
//
void Sensor::mainloop()
{
    // DEBUG -----
    p_tcp_server->register_callback_client_connected(std::bind(&Sensor::on_client_connected, this, std::placeholders::_1));
    while(!p_tcp_server->ready()) {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
    p_tcp_server->start_accepting_connections("8599");
    // DEBUG -----

    // DEBUG -----
    //p_gpio_manager->initialize();
    // DEBUG -----

    while (running)
    {
        // DEBUG -----
        //p_gpio_manager->toggle_led();
        //std::this_thread::sleep_for(std::chrono::milliseconds(800));
        // DEBUG -----

        //
        // implement state machine here
        //
        std::this_thread::sleep_for(std::chrono::milliseconds(SENSOR_MAINLOOP_INTERVAL));
    }
}

//
//  LISTENER THREAD
//
void Sensor::listener(int client_id)
{
    printf("SENSOR> new client: [%d]\n", client_id);
    // wait for commands in infinite loop
    while (running)
    {

        // DEBUG -----
        char buffer[20];
        int n = p_tcp_server->read(client_id, buffer, 3);       // blocking call
        buffer[3]='\0';
        if(n>0) { printf("SENSOR> received [%d] bytes from client %d: [%s]\n", n, client_id, buffer); }
        // terminate thread on client disconnected
        if(n==0){
            printf("TCPServer> client disconnected \n");
            break;
        }
        // DEBUG -----

    }
}


//
//  CALLBACK CLIENT CONNECTED
//
void Sensor::on_client_connected(int client_id)
{
    // store new client information
    // ...

    // spawn a listener thread
    p_listener_thread = new std::thread(&Sensor::listener, this, client_id);
}

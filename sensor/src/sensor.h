#pragma once

#include <thread>
#include <chrono>

//#include "structures.h"
#include "networking/tcpserver.h"       // TODO: "mlib/networking/tcpserver.h"
#include "networking/beacon.h"          // TODO: "mlib/networking/beacon.h"
#include "gpio/gpio_manager.h"          // TODO: "mraspi/gpio/gpio_manager.h"


class Sensor
{
public:
    Sensor();
    ~Sensor();
    // getters
    bool is_running() { return running; }

private:
    std::thread*         p_mainloop_thread;
    std::thread*         p_listener_thread;
    mlib::TCPServer*     p_tcp_server;          // TODO:  remove, for debug only
    mlib::Beacon*        p_beacon;
    mraspi::GPIOManager* p_gpio_manager;

    bool running;

    void mainloop();
    void listener(int client_id);
    void on_client_connected(int client_id);
};

